package com.khourssa.spring.boot.registration.project.controller;

import com.khourssa.spring.boot.registration.project.model.User;
import org.springframework.ui.Model;
import com.khourssa.spring.boot.registration.project.dto.UserDto;
import com.khourssa.spring.boot.registration.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
public class UserController {

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    private UserService userService;


    // Display registration page
    @GetMapping("/registration")
    public String getRegistrationPage(@ModelAttribute("user") UserDto userDto) {
        return "register";
    }


    // Save user after registration
    @PostMapping("/registration")
    public String saveUser(@ModelAttribute("user") UserDto userDto, Model model) {
        userService.save(userDto);
        return "redirect:/registration?success";
    }

    // Display login page
    @GetMapping("/login")
    public String login() {
        return "login";
    }


    // Display user page
    @GetMapping("/user-page")
    public String userPage (Model model, Principal principal) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(principal.getName());
        model.addAttribute("user", userDetails);
        return "user";
    }


    // Display list of users for admin
    @GetMapping("/users")
    public String ListUsers(Model model, Principal principal){
        UserDetails userDetails = userDetailsService.loadUserByUsername(principal.getName());
        model.addAttribute("user", userDetails);
        model.addAttribute("users", userService.getAllUsers());
        return "users";
    }

    // Display form to create a new user
    @GetMapping("/users/new")
    public String createUserForm(Model model) {

        // create user object to hold user form data
        User user = new User();
        model.addAttribute("user", user);
        return "create_user";

    }


    // Save new user
    @PostMapping("/users")
    public String saveUsernew(@ModelAttribute("user") User user, Model model) {
        userService.saveUsernew(user);
        return "redirect:/users";
    }

    // edit user for admins

    // Display form to edit a user
    @GetMapping("/users/edit/{id}")
    public String editUserForm(@PathVariable Long id, Model model) {
        model.addAttribute("user", userService.getUserById(id));
        return "edit_user";
    }

    // Update user
    @PostMapping("/users/{id}")
    public String updateUser(@PathVariable Long id,
                                @ModelAttribute("user") User user,
                                Model model) {

        // get user from database by id
        User existingUser = userService.getUserById(id);
        existingUser.setId(id);
        existingUser.setFullname(user.getFullname());
        existingUser.setRole(user.getRole());
        existingUser.setEmail(user.getEmail());

        // save updated user object
        userService.updateUser(existingUser);
        return "redirect:/users";
    }


    // Delete user
    @GetMapping("/users/{id}")
    public String deleteUser(@PathVariable Long id) {
        userService.deleteUserById(id);
        return "redirect:/users";
    }


    // Display form to edit user (for every user)
    @GetMapping("/user-page/edit/{id}")
    public String editUserFormCustom(@PathVariable Long id, Model model) {
        model.addAttribute("user", userService.getUserById(id));
        return "edit_userCustom";
    }
    // Update user (custom)
    @PostMapping("/user-page/{id}")
    public String updateUserCustom(@PathVariable Long id,
                             @ModelAttribute("user") User user,
                             Model model) {

        // get user from database by id
        User existingUser = userService.getUserById(id);
        existingUser.setId(id);
        existingUser.setFullname(user.getFullname());
        //existingUser.setRole(user.getRole());
        existingUser.setPassword(user.getPassword());
        existingUser.setEmail(user.getEmail());

        // save updated user object
        userService.updateUser(existingUser);
        return "redirect:/user-page";
    }

    // Search users by role
    @GetMapping("/users/search")
    public String searchUsersByRole(@RequestParam("role") String role, Model model, Principal principal) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(principal.getName());
        model.addAttribute("user", userDetails);
        model.addAttribute("users", userService.getUsersByRole(role));
        return "users";
    }










}
