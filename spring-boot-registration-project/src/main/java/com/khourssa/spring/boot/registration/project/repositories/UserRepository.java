package com.khourssa.spring.boot.registration.project.repositories;

import com.khourssa.spring.boot.registration.project.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail (String email);
    List<User> findByRole(String role);
}
