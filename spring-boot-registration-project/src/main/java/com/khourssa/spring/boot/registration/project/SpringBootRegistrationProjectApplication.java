package com.khourssa.spring.boot.registration.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRegistrationProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRegistrationProjectApplication.class, args);
	}

}
