package com.khourssa.spring.boot.registration.project.service;

import com.khourssa.spring.boot.registration.project.dto.UserDto;
import com.khourssa.spring.boot.registration.project.model.User;

import java.util.List;

public interface UserService {

    User save (UserDto userDto);

    // list all Employees methode
    List<User> getAllUsers();

    User saveUsernew(User user);

    //update users by id methods
    User getUserById(Long id);
    User updateUser(User user);

    // delete users by id method
    void deleteUserById(Long id);

    // methode pour afficher les users selon le Role
    public List<User> getUsersByRole(String role);
}
