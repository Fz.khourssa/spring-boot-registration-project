package com.khourssa.spring.boot.registration.project.service;


import com.khourssa.spring.boot.registration.project.model.User;
import com.khourssa.spring.boot.registration.project.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    // Load user by username (email)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // Find user in the database by email
        User user = userRepository.findByEmail(username);
        // If user is not found, throw UsernameNotFoundException
        if (user == null) {
            throw new UsernameNotFoundException("user not found");
        }

        // Return CustomUserDetail object which implements UserDetails
        return new CustomUserDetail(user);
    }
}
