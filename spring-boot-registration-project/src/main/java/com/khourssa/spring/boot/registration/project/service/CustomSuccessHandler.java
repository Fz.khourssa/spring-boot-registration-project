package com.khourssa.spring.boot.registration.project.service;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class CustomSuccessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {


        // Retrieve user's authorities
        var authourities = authentication.getAuthorities();
        // Extract the first authority (role) from the authorities
        var roles = authourities.stream().map(r -> r.getAuthority()).findFirst();

        // Redirect user based on their role
        if (roles.orElse("").equals("ADMIN")) {
            response.sendRedirect("/users"); // Redirect to admin page
        } else if (roles.orElse("").equals("USER")) {
            response.sendRedirect("/user-page");// Redirect to user page
        } else {
            response.sendRedirect("/error");// Redirect to error page if role is not recognized
        }



    }
}
