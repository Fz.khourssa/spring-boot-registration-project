package com.khourssa.spring.boot.registration.project.configurations;

import com.khourssa.spring.boot.registration.project.service.CustomSuccessHandler;
import com.khourssa.spring.boot.registration.project.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Autowired
    CustomSuccessHandler customSuccessHandler;

    @Autowired
    CustomUserDetailsService customUserDetailsService;


    // Defining a PasswordEncoder bean for password encoding
    @Bean
    public static PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    // Configuring HTTP security

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception{

        // Disabling CSRF protection
        http.csrf(c -> c.disable())

                // Configuring authorizations for different URLs
                .authorizeHttpRequests(request -> request.requestMatchers("/users")
                        .hasAuthority("ADMIN").requestMatchers("/user-page").hasAuthority("USER")
                        .requestMatchers("/registration", "/css/**").permitAll()
                        .anyRequest().authenticated())

                // Configuring login form
                .formLogin(form -> form.loginPage("/login").loginProcessingUrl("/login")
                        .successHandler(customSuccessHandler).permitAll())

                // Configuring logout
                .logout(form -> form.invalidateHttpSession(true).clearAuthentication(true)
                        .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                        .logoutSuccessUrl("/login?logout").permitAll());

        // Returning the configured security filter chain
        return http.build();

    }

    // Configuring authentication
    @Autowired
    public void configure (AuthenticationManagerBuilder auth) throws Exception {
        // Configuring authentication with custom service and password encoder
        auth.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoder());
    }
}
