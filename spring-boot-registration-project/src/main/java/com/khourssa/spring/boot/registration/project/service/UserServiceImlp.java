package com.khourssa.spring.boot.registration.project.service;

import com.khourssa.spring.boot.registration.project.dto.UserDto;
import com.khourssa.spring.boot.registration.project.model.User;
import com.khourssa.spring.boot.registration.project.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImlp implements UserService{



    private PasswordEncoder passwordEncoder;

    private UserRepository userRepository;

    public UserServiceImlp(PasswordEncoder passwordEncoder, UserRepository userRepository) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    public User save(UserDto userDto) {
        User user = new User(userDto.getEmail(), passwordEncoder.encode(userDto.getPassword()) , userDto.getRole(), userDto.getFullname());
        return userRepository.save(user);
    }


    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User saveUsernew(User user) {
        User usernew = new User(user.getEmail(), passwordEncoder.encode(user.getPassword()) , user.getRole(), user.getFullname());
        return userRepository.save(usernew);
    }

    @Override
    public User getUserById(Long id) {
        return userRepository.findById(id).get();
    }

    @Override
    public User updateUser(User user) {
        return userRepository.save(user);

    }

    @Override
    public void deleteUserById(Long id) {
        userRepository.deleteById(id);

    }

    @Override
    public List<User> getUsersByRole(String role) {
        return userRepository.findByRole(role);
    }

}
